<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('/home',function(){
    return view('index');
});

Route::get('/portfolio-1',function(){
    return view('portfolio-1');
});

Route::get('/portfolio-2',function(){
    return view('portfolio-1');
});
Route::get('/portfolio-3',function(){
    return view('portfolio-1');
});

Route::get('/index/{lang}',function($lang){
    App::setlocale($lang);
    return view('index');
});

Route::get('/resume/{lang}',function($lang){
    App::setlocale($lang);
    return view('/resume');
});
// Route::get('/index','HomeController@index');
